#!/usr/bin/perl

use Spreadsheet::WriteExcel;
use Spreadsheet::ParseExcel;
use Data::Dumper;
use Getopt::Long;
use DateTime;
use File::Slurp;


my $excelfile = "timetrack.xls";
my $workdatediff = "workdatediff";

my $hours = 0;
my $start = 0;
my $end = 0;
my $status = 0;

my $fmt = '%Y-%m-%dT%H:%M:%S.%3NZ';

GetOptions('hours=f' => \$hours, 'start' => \$start, 'end' => \$end, 'status' => \$status);

if($status) {

	if(-e $workdatediff) {
		my $startepoch = read_file($workdatediff);
		$startepoch =~ s/\R//g;
		my $startdt = DateTime->from_epoch( epoch => $startepoch );
		my $enddt = DateTime->now;

		my $duration = $enddt->subtract_datetime($startdt);

		my $status_time = $duration->in_units('minutes'); 

		if($status_time >= 60) {
				
			print sprintf("%.2f Hours worked\n", $status_time / 60);
		} else {
			my $status_time = $duration->in_units('minutes');
			print sprintf("%d Minutes worked\n", $status_time);
		}

		$startdt->set_time_zone('Europe/Berlin');

		print sprintf("started at %s\n", $startdt->hms(':'));

	} else {

		die "No startdate defined\n";
	}

}

if($end) {

	if(-e $workdatediff) {
		my $startepoch = read_file($workdatediff);
		$startepoch =~ s/\R//g;
		my $startdt = DateTime->from_epoch( epoch => $startepoch );
		my $enddt = DateTime->now;

		my $duration = $enddt->subtract_datetime($startdt);

		$hours = sprintf('%.2f', ( $duration->in_units('minutes') / 60 ) );

		unlink $workdatediff;
	} else {

		die "No startdate defined\n";
	}
}

if($hours) {

	my $lines = [];

	if(!(-f $excelfile)) {

		push @$lines, [ 'Datum', 'Stunden' ];
	} else {

		my $parser   = Spreadsheet::ParseExcel->new();
		my $workbook = $parser->parse($excelfile);

		if ( !defined $workbook ) {
			die $parser->error(), ".\n";
		}

		for my $worksheet ( $workbook->worksheets() ) {

			my ( $row_min, $row_max ) = $worksheet->row_range();
			my ( $col_min, $col_max ) = $worksheet->col_range();

			for my $row ( $row_min .. $row_max ) {

				my $line = [];

				for my $col ( $col_min .. $col_max ) {

					my $cell = $worksheet->get_cell( $row, $col );
					next unless $cell;

					push @$line, $cell->value;
				}

				push @$lines, $line;
			}
		}
	}

	push @$lines, [ DateTime->now->dmy, $hours ];

	print Dumper($lines);

	my $writebook = Spreadsheet::WriteExcel->new($excelfile);
	my $writesheet = $writebook->add_worksheet();

	my $row = 0;
	for my $line (@$lines) {

		$writesheet->write($row, 0, $line->[0]);
		$writesheet->write($row, 1, $line->[1]);
		$row++;
	}

}

if($start) {
	if(-f($workdatediff)) {
		print "Starttime already existing";
	} else {
		my $epoch = DateTime->now->epoch;
		write_file($workdatediff, $epoch);
	}
}


